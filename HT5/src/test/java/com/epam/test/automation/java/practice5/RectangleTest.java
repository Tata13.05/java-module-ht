package com.epam.test.automation.java.practice5;

import org.testng.Assert;

public class RectangleTest {
    @org.testng.annotations.Test
    public void test01() {
        Rectangle rectangle1 = new Rectangle(5, 10);
        double actual = rectangle1.getSideA();
        double expected = 5;

        Assert.assertEquals(actual, expected);
    }

    @org.testng.annotations.Test
    public void test02() {
        Rectangle rectangle2 = new Rectangle(6, 19);
        double actual = rectangle2.getSideB();
        double expected = 19;

        Assert.assertEquals(actual, expected);
    }
    @org.testng.annotations.Test
    public void test03() {
        Rectangle rectangle3 = new Rectangle(2, 13);
        double actual = rectangle3.area();
        double expected = 26;

        Assert.assertEquals(actual, expected);
    }
    @org.testng.annotations.Test
    public void test04() {
        Rectangle rectangle4 = new Rectangle(4, 3);
        double actual = rectangle4.perimeter();
        double expected = 14;

        Assert.assertEquals(actual, expected);
    }
    @org.testng.annotations.Test
    public void test05() {
        Rectangle rectangle5 = new Rectangle(5, 19);
        boolean actual = rectangle5.isSquare();
        boolean expected = false;

        Assert.assertEquals(actual, expected);
    }
    @org.testng.annotations.Test
    public void test06() {
        Rectangle rectangle6 = new Rectangle(2, 3);
        rectangle6.replaceSides();
        double sideA = rectangle6.getSideA();

        Assert.assertEquals(sideA, 3);
        Assert.assertEquals(rectangle6.getSideB(), 2);
    }

    @org.testng.annotations.Test
    public void test07() {
        Rectangle rectangle6 = new Rectangle();
        double sideA = rectangle6.getSideA();
        double sideB = rectangle6.getSideB();
        Assert.assertEquals(sideA, 4);
        Assert.assertEquals(sideB, 3);
    }
}