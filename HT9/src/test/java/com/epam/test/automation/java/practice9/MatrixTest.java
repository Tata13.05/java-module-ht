package com.epam.test.automation.java.practice9;

import org.testng.Assert;

import java.util.Arrays;


public class MatrixTest {

    @org.testng.annotations.Test
    public void testAddition() throws MatrixException {
        Matrix matrix1 = new Matrix(new double[][]{
                {1, 1, 1},
                {2, 2, 2},
                {3, 3, 3}});
        Matrix matrix2 = new Matrix(new double[][]{
                {1, 1, 1},
                {2, 2, 2},
                {3, 3, 3}});

        Matrix expected = new Matrix(new double[][]{
                {2, 2, 2},
                {4, 4, 4},
                {6, 6, 6}});
        double[][] expectedArray = expected.getArray();

        Matrix actual = matrix1.addition(matrix2);
        double[][] actualArray = actual.getArray();
        for (int i = 0; i < actualArray.length; i++) {
            Assert.assertTrue(Arrays.equals(actualArray[i], expectedArray[i]));

        }

    }


    @org.testng.annotations.Test
    public void testDeduction() throws MatrixException {
        Matrix matrix1 = new Matrix(new double[][]{
                {1, 1, 1},
                {2, 2, 2},
                {3, 3, 3}});
        Matrix matrix2 = new Matrix(new double[][]{
                {1, 1, 1},
                {2, 2, 2},
                {3, 3, 3}});

        Matrix expected = new Matrix(new double[][]{
                {0, 0, 0},
                {0, 0, 0},
                {0, 0, 0}});
        double[][] expectedArray = expected.getArray();

        Matrix actual = matrix1.deduction(matrix2);
        double[][] actualArray = actual.getArray();
        for (int i = 0; i < actualArray.length; i++) {
            Assert.assertTrue(Arrays.equals(actualArray[i], expectedArray[i]));

        }
    }

    @org.testng.annotations.Test
    public void testMultiplication() throws MatrixException {
        Matrix matrix1 = new Matrix(new double[][]{
                {1, 1, 1},
                {2, 2, 2},
                {3, 3, 3}});
        Matrix matrix2 = new Matrix(new double[][]{
                {1, 1, 1},
                {2, 2, 2},
                {3, 3, 3}});

        Matrix expected = new Matrix(new double[][]{
                {6, 6, 6},
                {12, 12, 12},
                {18, 18, 18}});
        double[][] expectedArray = expected.getArray();

        Matrix actual = matrix1.multiplication(matrix2);
        double[][] actualArray = actual.getArray();
        for (int i = 0; i < actualArray.length; i++) {
            Assert.assertTrue(Arrays.equals(actualArray[i], expectedArray[i]));
        }
    }
}