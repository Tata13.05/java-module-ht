package com.epam.test.automation.java.practice9;

public class Matrix {

    private final double[][] array;

    public double[][] getArray() {
        return array;
    }


    public Matrix(int rows, int columns) {
        this.array = new double[rows][columns];
    }

    public Matrix(double[][] array) {
        this.array = array;
    }

    public int getRows() {
        return array.length;
    }

    public int getColumns() {
        return array[0].length;
    }

    public void insertValue(double value, int row, int column) {
        array[row][column] = value;
    }

    public double getValue(int row, int column) {
        return array[row][column];
    }

    public Matrix addition(Matrix matrix) throws MatrixException {
        double[][] array1 = this.getArray();
        double[][] array2 = matrix.getArray();
        double[][] array3 = new double[getRows()][getColumns()];
        if (matrix.getRows() != this.getRows() || matrix.getColumns() != this.getColumns()) {
            throw new MatrixException();
        }
        for (int i = 0; i < getRows(); i++) {
            for (int j = 0; j < getColumns(); j++) {
                array3[i][j] = array1[i][j] + array2[i][j];
            }
        }
        return new Matrix(array3);
    }

    public Matrix deduction(Matrix matrix) throws MatrixException {
        double[][] array1 = this.getArray();
        double[][] array2 = matrix.getArray();
        double[][] array3 = new double[getRows()][getColumns()];
        if (matrix.getRows() != this.getRows() || matrix.getColumns() != this.getColumns()) {
            throw new MatrixException();
        }
        for (int i = 0; i < getRows(); i++) {
            for (int j = 0; j < getColumns(); j++) {
                array3[i][j] = array1[i][j] - array2[i][j];
            }
        }
        return new Matrix(array3);
    }

    public Matrix multiplication(Matrix matrix) throws MatrixException {

        double[][] array1 = this.getArray();
        double[][] array2 = matrix.getArray();
        double[][] array3 = new double[getRows()][getColumns()];
        if (matrix.getRows() != this.getColumns() || matrix.getColumns() != this.getRows()) {
            throw new MatrixException();
        }
        for (int i = 0; i < getRows(); i++) {
            for (int j = 0; j < this.getColumns(); j++) {
                for (int k = 0; k < this.getRows(); k++) {
                    array3[i][j] += array1[i][k] * array2[k][j];
                }
            }

        }
        return new Matrix(array3);
    }
}


