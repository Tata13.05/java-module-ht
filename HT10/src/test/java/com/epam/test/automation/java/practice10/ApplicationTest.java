package com.epam.test.automation.java.practice10;

import org.testng.Assert;

import java.util.Arrays;
import java.util.List;


public class ApplicationTest {

    @org.testng.annotations.Test
    public void testTask1() {
        List<String> resultList = Application.task1('a', Arrays.asList("Hello", "qwerty", "asda", "asdfa", "as", "a"));
        List<String> expected = Arrays.asList("asda", "asdfa");
        Assert.assertEquals(resultList, expected);
    }

    @org.testng.annotations.Test
    public void testTask2() {
        List<Integer> result = Application.task2(Arrays.asList("Hello", "world", "!", "Good", "morning", "!"));
        List<Integer> expected = Arrays.asList(1, 1, 4, 5, 5, 7);
        Assert.assertEquals(result, expected);
    }

    @org.testng.annotations.Test
    public void testTask3() {
        List<String> result = Application.task3(Arrays.asList("asd", "a", "basdw"));
        List<String> expected = Arrays.asList("ad", "aa", "bw");
        Assert.assertEquals(result, expected);
    }

    @org.testng.annotations.Test
    public void testTask4() {
        List<String> result = Application.task4(2, Arrays.asList("8DC3", "4F", "B", "3S", "S3", "A1", "2A3G", "1B"));
        List<String> expected = Arrays.asList("A1", "S3");
        Assert.assertEquals(result, expected);
    }

    @org.testng.annotations.Test
    public void testTask5() {
        List<Integer> result = Application.task5(Arrays.asList(1, 2, 3, 4, 5, 6));
        List<Integer> expected = Arrays.asList(1, 3, 5);
        Assert.assertEquals(result, expected);
    }

    @org.testng.annotations.Test
    public void testTask6() {
        List<String> result = Application.task6(Arrays.asList(1, 3, 4), Arrays.asList("1aa", "aaa", "1", "a"));
        List<String> expected = Arrays.asList("1", "1aa", "Not Found");
        Assert.assertEquals(result, expected);
    }

    @org.testng.annotations.Test
    public void testTask7() {
        List<Integer> result = Application.task7(5, Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));
        List<Integer> expected = Arrays.asList(9, 7, 4, 2);
        Assert.assertEquals(result, expected);
    }

    @org.testng.annotations.Test
    public void testTask8() {
        List<Integer> result = Application.task8(3, 4, Arrays.asList(-10, 3, -3, 4, 55, 6));
        List<Integer> expected = Arrays.asList(55, 6, 4);
        Assert.assertEquals(result, expected);
    }

    @org.testng.annotations.Test
    public void testTask9() {
        List<String> result = Application.task9(Arrays.asList("ABC", "A", "BCD", "D"));
        List<String> expected = Arrays.asList("4-A", "3-B", "1-D");
        Assert.assertEquals(result, expected);
    }

    @org.testng.annotations.Test
    public void testTask10() {
        List<Character> result = Application.task10(Arrays.asList("asnsbiu", "asdsad", "asnsb", "asdf", "asdfb", "as", "a", "aop"));
        List<Character> expected = Arrays.asList('U', 'D', 'B', 'B', 'F', 'P', 'S', 'A');
        Assert.assertEquals(result, expected);
    }

    @org.testng.annotations.Test
    public void testTask11() {
        List<YearSchoolStat> result = Application.task11(Arrays.asList(
                new Entrant(1, 1993, "Ivanov"),
                new Entrant(2, 1992, "Petrov"),
                new Entrant(3, 1993, "Pupkin"),
                new Entrant(3, 2000, "Zobkin"),
                new Entrant(3, 2000, "Zabkin")));
        List<YearSchoolStat> expected = Arrays.asList(
                new YearSchoolStat(1992, 1),
                new YearSchoolStat(2000, 1),
                new YearSchoolStat(1993, 2));
        Assert.assertEquals(result, expected);

    }
}