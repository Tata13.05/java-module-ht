package com.epam.test.automation.java.practice10;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Application {
    public static List<String> task1(char c, List<String> stringList) {
        return stringList.stream()
                .filter(s -> s.length() > 1)
                .filter(s -> s.startsWith(String.valueOf(c)))
                .filter(s -> s.endsWith(String.valueOf(c)))
                .collect(Collectors.toList());
    }

    public static List<Integer> task2(List<String> integerList) {
        return integerList.stream()
                .map(s -> s.length())
                .sorted()
                .collect(Collectors.toList());
    }

    public static List<String> task3(List<String> stringList) {
        return stringList.stream()
                .map(s -> "" + s.charAt(0) + s.charAt(s.length() - 1))
                .collect(Collectors.toList());
    }

    public static List<String> task4(int k, List<String> stringList) {
        return stringList.stream()
                .filter(s -> s.length() == k)
                .filter(s -> Character.isDigit(s.charAt(s.length() - 1)))
                .sorted()
                .collect(Collectors.toList());
    }

    public static List<Integer> task5(List<Integer> integerListList) {
        return integerListList.stream()
                .filter(i -> i % 2 != 0)
                .sorted()
                .collect(Collectors.toList());
    }

    public static List<String> task6(List<Integer> integerList, List<String> stringList) {
        return integerList.stream()
                .map(integer -> stringList.stream()
                        .filter(s -> Character.isDigit(s.charAt(0)))
                        .filter(s -> s.length() == integer)
                        .findFirst().orElse("Not Found"))
                .collect(Collectors.toList());
    }

    public static List<Integer> task7(int k, List<Integer> integerList) {
        return integerList.stream()
                .filter(i -> i % 2 == 0 ^ integerList.subList(k, integerList.size()).contains(i))
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList());
    }

    public static List<Integer> task8(int k, int d, List<Integer> integerList) {
        return integerList.stream()
                .filter(i -> i > k && integerList.size() > d)
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList());
    }

    public static List<String> task9(List<String> stringList) {
        Comparator<String> comparator1 = Comparator.comparing(s -> -s.charAt(0));
        Comparator<String> comparator2 = Comparator.comparing(s -> s.charAt(s.length() - 1));
        return stringList.stream()
                .collect(Collectors.groupingBy(s -> s.charAt(0)))
                .entrySet().stream()
                .map(entry -> String.join("", entry.getValue()).length() + "-" + entry.getKey())
                .sorted(comparator1.thenComparing(comparator2))
                .collect(Collectors.toList());
    }

    public static List<Character> task10(List<String> stringList) {
        return stringList.stream()
                .sorted(Comparator.comparingInt(String::length).reversed())
                .map(s -> s.toUpperCase().charAt(s.length() - 1))
                .collect(Collectors.toList());
    }

    public static List<YearSchoolStat> task11(List<Entrant> entrantList) {
        Comparator<YearSchoolStat> comparator1 = Comparator.comparing(y -> y.numberOfSchools);
        Comparator<YearSchoolStat> comparator2 = Comparator.comparing(y -> y.yearOfEntering);
        return entrantList.stream()
                .collect(Collectors.groupingBy(e -> e.yearOfEntering))
                .entrySet().stream()
                .map(entry -> new YearSchoolStat(entry.getKey(), (int) entry.getValue().stream()
                        .map(o -> o.schoolNumber)
                        .distinct()
                        .count()))
                .sorted(comparator1.thenComparing(comparator2))
                .collect(Collectors.toList());


    }
}


