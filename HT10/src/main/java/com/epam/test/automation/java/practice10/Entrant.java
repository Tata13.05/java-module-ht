package com.epam.test.automation.java.practice10;

public class Entrant {
    int schoolNumber;
    int yearOfEntering;
    String lastName;

    public Entrant(int schoolNumber, int yearOfEntering, String lastName) {
        this.schoolNumber = schoolNumber;
        this.yearOfEntering = yearOfEntering;
        this.lastName = lastName;
    }

}
