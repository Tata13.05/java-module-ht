package com.epam.test.automation.java.practice10;

import java.util.Objects;

public class YearSchoolStat {
    int yearOfEntering;
    int numberOfSchools;

    public YearSchoolStat(int yearOfEntering, int numberOfSchools) {
        this.yearOfEntering = yearOfEntering;
        this.numberOfSchools = numberOfSchools;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        YearSchoolStat that = (YearSchoolStat) o;
        return yearOfEntering == that.yearOfEntering && numberOfSchools == that.numberOfSchools;
    }

    @Override
    public int hashCode() {
        return Objects.hash(yearOfEntering, numberOfSchools);
    }
}
