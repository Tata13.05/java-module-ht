package com.epam.test.automation.java.practice6;

import java.math.BigDecimal;

/**
 * <summary>
 * Implement code according to description of task.
 * </summary>
 */
public class Manager extends Employee {
    private int quantity;

    public Manager(String name, BigDecimal salary, int clientAmount) {
        super(name, salary);
        this.quantity = clientAmount;
    }

    @Override
    public void setBonus(BigDecimal bonus) {
        if (quantity > 100) {
            setBonus(bonus.add(BigDecimal.valueOf(500)));
        }
        if (quantity > 150) {
            setBonus(bonus.add(BigDecimal.valueOf(1000)));
        }
    }
}
