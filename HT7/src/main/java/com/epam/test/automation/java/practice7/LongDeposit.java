package com.epam.test.automation.java.practice7;

import java.math.BigDecimal;

public class LongDeposit extends Deposit {
    public LongDeposit(BigDecimal amount, int period) {
        super(amount, period);
    }

    @Override
    BigDecimal income() {
        BigDecimal longDepositAmount = getAmount();
        int monthNumber = getPeriod();
        BigDecimal totalSum = longDepositAmount;
        for (int i = 6; i < monthNumber; i++) {
            totalSum = totalSum.multiply(BigDecimal.valueOf(1.15));

        }
        return totalSum.subtract(longDepositAmount);
    }

}
