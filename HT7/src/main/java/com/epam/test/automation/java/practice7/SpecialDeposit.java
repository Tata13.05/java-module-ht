package com.epam.test.automation.java.practice7;

import java.math.BigDecimal;

public class SpecialDeposit extends Deposit {
    public SpecialDeposit(BigDecimal amount, int period) {
        super(amount, period);
    }

    @Override
    BigDecimal income() {
        BigDecimal specialDepositAmount = getAmount();
        int monthNumber = getPeriod();
        BigDecimal totalSum = specialDepositAmount;
        for (int i = 0; i < monthNumber; i++) {
            totalSum = totalSum.multiply(BigDecimal.valueOf(1 + 0.01 * i));

        }
        return totalSum.subtract(specialDepositAmount);
    }

}