package com.epam.test.automation.java.practice7;

import java.math.BigDecimal;

public class Client {
    private final Deposit[] deposits;

    public Client() {
        this.deposits = new Deposit[10];
    }

    public boolean addDeposit(Deposit deposit) {
        for (int q = 0; q < deposits.length; q++) {
            if (deposits[q] == null) {
                deposits[q] = deposit;
                return true;
            }
        }
        return false;
    }

    public BigDecimal totalIncome() {
        BigDecimal totalIncome = new BigDecimal(0);
        for (Deposit d : deposits) {
            BigDecimal income = d.income();
            totalIncome = income.add(totalIncome);
        }
        return totalIncome;
    }

    public BigDecimal maxIncome() {
        BigDecimal maxIncome = new BigDecimal(0);
        for (Deposit d : deposits) {
            BigDecimal currentIncome = d.income();
            int compareTo = currentIncome.compareTo(maxIncome);
            if (compareTo > 0) {
                maxIncome = currentIncome;
            }
        }
        return maxIncome;
    }

    public BigDecimal getIncomeByNumber(int number) {
        Deposit deposit = deposits[number];
        if (deposits[number] == null) {
            return new BigDecimal(0);
        }
        return deposit.income();
    }
}