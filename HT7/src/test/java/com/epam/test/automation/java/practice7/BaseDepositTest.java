package com.epam.test.automation.java.practice7;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.math.BigDecimal;

public class BaseDepositTest {

    @Test
    public void test01() {
        Deposit baseDepositAmount = new BaseDeposit(BigDecimal.valueOf(1000000), 5);
        BigDecimal actual = baseDepositAmount.income();
        BigDecimal expected = BigDecimal.valueOf(276281.56);
        Assert.assertEquals(actual, expected);
    }
}
