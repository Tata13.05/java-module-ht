package com.epam.test.automation.java.practice7;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.math.BigDecimal;

public class LongDepositTest {

    @Test
    public void test01() {
        Deposit longDepositAmount = new LongDeposit(BigDecimal.valueOf(1000000), 4);
        BigDecimal actual = longDepositAmount.income();
        BigDecimal expected = BigDecimal.valueOf(0);
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void test02() {
        Deposit longDepositAmount = new LongDeposit(BigDecimal.valueOf(1000000), 10);
        BigDecimal actual = longDepositAmount.income();
        BigDecimal expected = BigDecimal.valueOf(749006.25);
        Assert.assertEquals(actual.compareTo(expected), 0);
    }
}
