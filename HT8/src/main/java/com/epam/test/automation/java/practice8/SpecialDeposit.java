package com.epam.test.automation.java.practice8;

import java.math.BigDecimal;

/**
 * <summary>
 * Implement class according to description of task.
 * </summary>
 */
public class SpecialDeposit extends Deposit implements Prolongable {
    public SpecialDeposit(BigDecimal amount, int period) {
        super(amount, period);
    }


    @Override
    BigDecimal income() {
        BigDecimal specialDepositAmount = getAmount();
        int monthNumber = getPeriod();
        BigDecimal totalSum = specialDepositAmount;
        for (int i = 0; i < monthNumber; i++) {
            totalSum = totalSum.multiply(BigDecimal.valueOf(1 + 0.01 * i));

        }
        return totalSum.subtract(specialDepositAmount);
    }

    @Override
    public boolean canToProlong() {
        return amount.compareTo(BigDecimal.valueOf(1000)) > 0;
    }
}