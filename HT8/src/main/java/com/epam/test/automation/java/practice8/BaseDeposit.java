package com.epam.test.automation.java.practice8;

import java.math.BigDecimal;

/**
 * <summary>
 * Implement class according to description of task.
 * </summary>
 */
public class BaseDeposit extends Deposit {
    public BaseDeposit(BigDecimal amount, int period) {
        super(amount, period);
    }

    @Override
    public BigDecimal income() {
        BigDecimal baseDepositAmount = getAmount();
        int monthNumber = getPeriod();
        BigDecimal totalSum = baseDepositAmount;
        for (int i = 0; i < monthNumber; i++) {
            totalSum = totalSum.multiply(BigDecimal.valueOf(1.05));

        }
        return totalSum.subtract(baseDepositAmount).setScale(2, BigDecimal.ROUND_DOWN);
    }

}
