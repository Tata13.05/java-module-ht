package com.epam.test.automation.java.practice8;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * <summary>
 * Implement class according to description of task.
 * </summary>
 */
public class Client implements Iterable<Deposit> {
    private final Deposit[] deposits;

    public Client() {
        this.deposits = new Deposit[10];
    }

    public boolean addDeposit(Deposit deposit) {
        for (int q = 0; q < deposits.length; q++) {
            if (deposits[q] == null) {
                deposits[q] = deposit;
                return true;
            }
        }
        return false;
    }

    public BigDecimal totalIncome() {
        BigDecimal totalIncome = new BigDecimal(0);
        for (Deposit d : deposits) {
            BigDecimal income = d.income();
            totalIncome = income.add(totalIncome);
        }
        return totalIncome;
    }

    public BigDecimal maxIncome() {
        BigDecimal maxIncome = new BigDecimal(0);
        for (Deposit d : deposits) {
            BigDecimal currentIncome = d.income();
            int compareTo = currentIncome.compareTo(maxIncome);
            if (compareTo > 0) {
                maxIncome = currentIncome;
            }
        }
        return maxIncome;
    }

    public BigDecimal getIncomeByNumber(int number) {
        Deposit deposit = deposits[number];
        if (deposits[number] == null) {
            return new BigDecimal(0);
        }
        return deposit.income();
    }

    @Override
    public Iterator<Deposit> iterator() {
        return new Iterator<Deposit>() {
            int i = 0;

            @Override
            public boolean hasNext() {
                return i < deposits.length && deposits[i] != null;
            }

            @Override
            public Deposit next() {
                if (hasNext()) {
                    Deposit deposit = deposits[i];
                    i++;
                    return deposit;
                }
                throw new NoSuchElementException();
            }
        };
    }

    public void sortDeposits() {
        for (int i = 0; i < deposits.length; i++) {
            for (int j = 0; j < deposits.length - 1; j++) {

                Deposit current = deposits[j];
                Deposit next = deposits[j + 1];

                if (next.compareTo(current) > 0) {
                    deposits[j + 1] = current;
                    deposits[j] = next;
                }
            }
        }
    }

    public int countPossibleToProlongDeposit() {
        int counter = 0;
        for (Deposit d : deposits) {
            if (d instanceof Prolongable) {
                Prolongable p = (Prolongable) d;
                if (p.canToProlong()) {
                    counter++;
                }
            }
        }
        return counter;
    }
}