package com.epam.test.automation.java.practice8;

import java.math.BigDecimal;

/**
 * <summary>
 * Implement class according to description of task.
 * </summary>
 */
public class LongDeposit extends Deposit implements Prolongable {
    public LongDeposit(BigDecimal amount, int period) {
        super(amount, period);
    }

    @Override
    public boolean canToProlong() {
        return period <= 36;
    }

    @Override
    BigDecimal income() {
        BigDecimal longDepositAmount = getAmount();
        int monthNumber = getPeriod();
        BigDecimal totalSum = longDepositAmount;
        for (int i = 6; i < monthNumber; i++) {
            totalSum = totalSum.multiply(BigDecimal.valueOf(1.15));

        }
        return totalSum.subtract(longDepositAmount);
    }

}