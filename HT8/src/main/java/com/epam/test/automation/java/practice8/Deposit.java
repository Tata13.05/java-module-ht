package com.epam.test.automation.java.practice8;

import java.math.BigDecimal;

/**
 * <summary>
 * Implement class according to description of task.
 * </summary>
 */
public abstract class Deposit implements Comparable<Deposit> {
    public BigDecimal amount;
    public int period;

    public Deposit(BigDecimal depositAmount, int depositPeriod) {
        this.amount = depositAmount;
        this.period = depositPeriod;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public int getPeriod() {
        return period;
    }

    abstract BigDecimal income();


    @Override
    public int compareTo(Deposit deposit) {
        BigDecimal totalSumAmount = this.amount.add(income());
        BigDecimal totalSumAmount1 = deposit.amount.add(deposit.income());

        return totalSumAmount.compareTo(totalSumAmount1);
    }
}
